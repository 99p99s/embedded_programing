#-------------------------------------------------------------------------------
# @file cookIt.mk
#
# INCLUDE THIS FILE IN YOUR MAKEFILE USING THE ...
#
#   include <path/to/>cookIt.mk
#
# ... MAKEFILE DIRECTIVE.
#
# There are the following options:
#
#   make all      : Everything except uploading
#   make binary   : Makes the binary that can be uploaded (default firmware.bin)
#   make upload   : Uploads to the DUE using openOCD
#   make clean    : Cleans up the "$OUTPUT_DIR/build" or "$OUTPUT_DIR/debug"
#
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# CONFIG: OVERWRITE as params to modify
#-------------------------------------------------------------------------------
.SUFFIXES: .o .a .c .s
SHELL=/bin/sh
TARGET_NAME=firmware
OUTPUT_DIR=.
PROJECT_DIR=./src
OPENOCD=/usr/bin/openocd


#-------------------------------------------------------------------------------
# Related directories and files
#-------------------------------------------------------------------------------
ROOT:=$(realpath $(shell dirname '$(dir $(lastword $(MAKEFILE_LIST)))'))
vpath %.c $(PROJECT_DIR)
VPATH+=$(PROJECT_DIR)

INCLUDES =
INCLUDES += -I$(ROOT)/include
INCLUDES += -I$(ROOT)/sam
INCLUDES += -I$(ROOT)/sam/libsam
INCLUDES += -I$(ROOT)/sam/CMSIS/CMSIS/Include
INCLUDES += -I$(ROOT)/sam/CMSIS/Device/ATMEL


#-------------------------------------------------------------------------------
# Target selection
#-------------------------------------------------------------------------------
ifdef DEBUG
	OPTIMIZATION = -g -O0 -DDEBUG
	OBJ_DIR = debug
else
	OPTIMIZATION = -Os
	OBJ_DIR = build
endif


#-------------------------------------------------------------------------------
#  Toolchain
#-------------------------------------------------------------------------------
CROSS_COMPILE = $(ROOT)/tools/bin/arm-none-eabi-
AR = $(CROSS_COMPILE)ar
CC = $(CROSS_COMPILE)gcc
DB = $(CROSS_COMPILE)gdb
CXX = $(CROSS_COMPILE)g++
AS = $(CROSS_COMPILE)as
NM = $(CROSS_COMPILE)nm
LKELF = $(CROSS_COMPILE)g++
OBJCP = $(CROSS_COMPILE)objcopy
RM = rm -Rf
MKDIR = mkdir -p


#-------------------------------------------------------------------------------
#  Flags
#-------------------------------------------------------------------------------
CFLAGS += -Wall --param max-inline-insns-single=500 -mcpu=cortex-m3 -mthumb 
CFLAGS += -mlong-calls -ffunction-sections -fdata-sections -nostdlib -std=c99
CFLAGS += $(OPTIMIZATION) $(INCLUDES)
#CFLAGS += -Dprintf=iprintf
CPPFLAGS += -Wall --param max-inline-insns-single=500 -mcpu=cortex-m3 -mthumb
CPPFLAGS += -mlong-calls -nostdlib
CPPFLAGS += -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -std=c++98
CPPFLAGS += $(OPTIMIZATION) $(INCLUDES)
#CPPFLAGS += -Dprintf=iprintf
ASFLAGS = -mcpu=cortex-m3 -mthumb -Wall -g $(OPTIMIZATION) $(INCLUDES)
ARFLAGS = rcs

# High verbosity flags
ifdef VERBOSE
	CFLAGS += -Wall -Wchar-subscripts -Wcomment -Wformat=2 -Wimplicit-int
	CFLAGS += -Werror-implicit-function-declaration -Wmain -Wparentheses
	CFLAGS += -Wsequence-point -Wreturn-type -Wswitch -Wtrigraphs -Wunused
	CFLAGS += -Wuninitialized -Wunknown-pragmas -Wfloat-equal -Wundef
	CFLAGS += -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings
	CFLAGS += -Wsign-compare -Waggregate-return -Wstrict-prototypes
	CFLAGS += -Wmissing-prototypes -Wmissing-declarations
	CFLAGS += -Wformat -Wmissing-format-attribute -Wno-deprecated-declarations
	CFLAGS += -Wredundant-decls -Wnested-externs -Winline -Wlong-long
	CFLAGS += -Wunreachable-code
	CFLAGS += -Wcast-align
	CFLAGS += -Wmissing-noreturn
	CFLAGS += -Wconversion
	CPPFLAGS += -Wall -Wchar-subscripts -Wcomment -Wformat=2
	CPPFLAGS += -Wmain -Wparentheses -Wcast-align -Wunreachable-code
	CPPFLAGS += -Wsequence-point -Wreturn-type -Wswitch -Wtrigraphs -Wunused
	CPPFLAGS += -Wuninitialized -Wunknown-pragmas -Wfloat-equal -Wundef
	CPPFLAGS += -Wshadow -Wpointer-arith -Wwrite-strings
	CPPFLAGS += -Wsign-compare -Waggregate-return -Wmissing-declarations
	CPPFLAGS += -Wformat -Wmissing-format-attribute -Wno-deprecated-declarations
	CPPFLAGS += -Wpacked -Wredundant-decls -Winline -Wlong-long
	CPPFLAGS += -Wmissing-noreturn
	CPPFLAGS += -Wconversion
	UPLOAD_VERBOSE_FLAGS += -i -d
endif

# Flags for linking stage
LNK_SCRIPT=$(ROOT)/sam/linker_scripts/gcc/flash.ld
LIBSAM_ARCHIVE=$(ROOT)/lib/libsam_sam3x8e_gcc_rel.a


#-------------------------------------------------------------------------------
# Source files and objects
#-------------------------------------------------------------------------------
#  builds a list of all the .c source files in two locations:
#    The current project directory ($(PROJECT_DIR))
#    A subdirectory called src located one level above the current project directory (../src)
#C_SRC=$(wildcard $(PROJECT_DIR)/*.c) $(wildcard ../src/*.c)
C_SRC=$(wildcard $(PROJECT_DIR)/*.c)
# C_SRC contains src/file1.c src/file2.c. This line transforms that list into file1.o 
# file2.o. It removes the directory paths and replaces the .c extension with .o, essentially 
# creating a list of object file names corresponding to the source files in C_SRC.
C_OBJ=$(patsubst %.c, %.o, $(notdir $(C_SRC)))
CPP_SRC=$(wildcard $(PROJECT_DIR)/*.cpp)
CPP_OBJ=$(patsubst %.cpp, %.o, $(notdir $(CPP_SRC)))
A_SRC=$(wildcard $(PROJECT_DIR)/*.s)
A_OBJ=$(patsubst %.s, %.o, $(notdir $(A_SRC)))


#-------------------------------------------------------------------------------
# Rules
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
all: binary

#-------------------------------------------------------------------------------
.PHONY: clean
clean:
	-@$(RM) $(OBJ_DIR) 1>/dev/null 2>&1
	-@$(RM) $(OUTPUT_DIR)/$(TARGET_NAME) 1>/dev/null 2>&1

#-------------------------------------------------------------------------------
.PHONY: prepare
prepare:
	-@$(MKDIR) $(OBJ_DIR) 1>/dev/null 2>&1

#-------------------------------------------------------------------------------
.PHONY: binary
binary: prepare $(OBJ_DIR)/$(TARGET_NAME).elf
	THE_FILE="$(addprefix $(OBJ_DIR)/, $(TARGET_NAME)).elf"

#-------------------------------------------------------------------------------
# .elf ------> UPLOAD TO CONTROLLER
.PHONY: upload
upload: binary
	@echo "Uploading ..."
	$(OPENOCD) -s /usr/share/openocd/scripts \
               -f interface/cmsis-dap.cfg -f board/atmel_sam3x_ek.cfg \
               -c "init" \
               -c "halt" \
               -c "flash write_image erase $(THE_FILE)" \
               -c "at91sam3 gpnvm set 1" \
               -c "exit"
	@echo "Done."

#-------------------------------------------------------------------------------
# .c -> .o
$(addprefix $(OBJ_DIR)/,$(C_OBJ)): $(notdir $(C_SRC))
	"$(CC)" -c $(CFLAGS) $< -o $@

# .o -> .a
$(OBJ_DIR)/$(TARGET_NAME).a: $(addprefix $(OBJ_DIR)/, $(C_OBJ))
	"$(AR)" $(ARFLAGS) $@ $^
	"$(NM)" $@ > $@.txt

#  -> .elf
$(OBJ_DIR)/$(TARGET_NAME).elf: $(OBJ_DIR)/$(TARGET_NAME).a
	"$(LKELF)" -Os -Wl,--gc-sections -mcpu=cortex-m3 \
	  "-T$(LNK_SCRIPT)" "-Wl,-Map,$(OBJ_DIR)/$(TARGET_NAME).map" \
	  -o $@ \
	  "-L$(OBJ_DIR)" \
	  -lm -lgcc -mthumb -Wl,--cref -Wl,--check-sections -Wl,--gc-sections \
	  -Wl,--entry=Reset_Handler -Wl,--unresolved-symbols=report-all -Wl,--warn-common \
	  -Wl,--warn-section-align -Wl,--warn-unresolved-symbols \
	  -Wl,--start-group \
	  $^ $(LIBSAM_ARCHIVE) \
	  -Wl,--end-group

# .elf -> .bin
$(OBJ_DIR)/$(TARGET_NAME).bin: $(OBJ_DIR)/$(TARGET_NAME).elf
	"$(OBJCP)" -O binary $< $@